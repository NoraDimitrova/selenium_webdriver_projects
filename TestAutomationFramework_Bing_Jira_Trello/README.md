**Instructions**

1. Clone repository and open project TestAutomationFrameworkNora;

2. To run Jira test put your email and password in resources/config.properties here:
  -  `jira.users.user.username=email`
  -  `jira.users.user.password=password`

3. Put summary and description for Story and Bug in resources/config.properties here:
  -  jira.users.user.summaryStory=
  -  jira.users.user.descriptionStory= 
  -  jira.users.user.summaryBug= 
  -  jira.users.user.descriptionBug=

4. In page NavigateToProject put the XPath of your project - for example:

  -  actions.waitForElementVisible("//span[@style='background-image: url(\"https://noradimitrova.atlassian." +
   "net/rest/api/2/universal_avatar/view/type/project/avatar/10419?size=medium\"); border-radius: 2px;']");
  -  actions.clickElement("//span[@style='background-image: url(\"https://noradimitrova.atlassian.net/rest/api/2" +
   "/universal_avatar/view/type/project/avatar/10419?size=medium\"); border-radius: 2px;']");


5. To run Trello test put your email and password in resources/config.properties here:
  - `trello.users.user.username=email`
  - `trello.users.user.password=password`