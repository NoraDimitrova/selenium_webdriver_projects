package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class CreateStory extends BaseJiraPage {

    public CreateStory(WebDriver driver) {
        super(driver, "jira.navigateToProjectUrl");
    }


    public void createStory(String user) {
        String summaryStory = Utils.getConfigPropertyByKey("jira.users." + user + ".summaryStory");
        String descriptionStory = Utils.getConfigPropertyByKey("jira.users." + user + ".descriptionStory");

        actions.waitForElementClickable("jira.createStoryAndBug.createButton");
        actions.clickElement("jira.createStoryAndBug.createButton");

        actions.waitForElementClickable("jira.createStoryAndBug.issueTypeField");
        actions.clickElement("jira.createStoryAndBug.issueTypeField");
        actions.clickElement("jira.createStory.issueStory");

        actions.waitForElementClickable("jira.create.summaryField");
        actions.clickElement("jira.create.summaryField");
        actions.typeValueInField(summaryStory, "jira.create.summaryField");

        actions.waitForElementVisible("jira.create.descriptionField");
        actions.clickElement("jira.create.descriptionField");
        actions.typeValueInField(descriptionStory, "jira.create.descriptionField");

        actions.waitForElementClickable("jira.create.priorityMedium");
        actions.clickElement("jira.create.priorityMedium");
        actions.waitForElementClickable("jira.create.priorityLow");
        actions.clickElement("jira.create.priorityLow");

        actions.waitForElementClickable("jira.create.buttonCreateIssue");
        actions.clickElement("jira.create.buttonCreateIssue");
//copy issue
//        actions.waitForElementPresent("//span[contains(text(), 'Copy link')]");
//        actions.clickElement("//span[contains(text(), 'Copy link')]");

        actions.waitForElementVisible("//div[@data-testid='create-button-wrapper']");
        actions.assertElementPresent("//div[@data-testid='create-button-wrapper']");


    }
}
