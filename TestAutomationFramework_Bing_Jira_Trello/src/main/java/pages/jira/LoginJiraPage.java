package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginJiraPage extends BaseJiraPage {


    public LoginJiraPage(WebDriver driver) {
        super(driver, "jira.url");
    }

    public void loginJira(String user) {
        String username = Utils.getConfigPropertyByKey("jira.users." + user + ".username");
        String password = Utils.getConfigPropertyByKey("jira.users." + user + ".password");

        actions.waitForElementClickable("jira.loginJiraPage.myAccountButton");
        actions.clickElement("jira.loginJiraPage.myAccountButton");
        actions.clickElement("jira.loginJiraPage.findMyTeam");
        actions.typeValueInField(username, "jira.loginJiraPage.emailField");
        actions.clickElement("jira.loginJiraPage.clickButton");
        actions.waitForElementClickable("jira.loginJiraPage.passwordField");

        actions.typeValueInField(password, "jira.loginJiraPage.passwordField");
        actions.clickElement("jira.loginJiraPage.clickButton");

    }
}
