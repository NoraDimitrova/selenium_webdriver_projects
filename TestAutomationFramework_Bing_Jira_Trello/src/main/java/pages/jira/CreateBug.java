package pages.jira;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class CreateBug extends BaseJiraPage {

    public CreateBug(WebDriver driver) {
        super(driver, "jira.url");
    }

    public void createBug(String user){
        String summaryBug = Utils.getConfigPropertyByKey("jira.users." + user + ".summaryBug");
        String descriptionBug = Utils.getConfigPropertyByKey("jira.users." + user + ".descriptionBug");


        actions.waitForElementClickable("jira.createStoryAndBug.createButton");
        actions.clickElement("jira.createStoryAndBug.createButton");

        actions.waitForElementClickable("jira.createStoryAndBug.issueTypeField");
        actions.clickElement("jira.createStoryAndBug.issueTypeField");
        actions.clickElement("jira.createStory.issueBug");

        actions.waitForElementClickable("jira.create.summaryField");
        actions.clickElement("jira.create.summaryField");
        actions.typeValueInField(summaryBug, "jira.create.summaryField");

        actions.waitForElementClickable("jira.create.descriptionField");
        actions.clickElement("jira.create.descriptionField");
        actions.typeValueInField(descriptionBug, "jira.create.descriptionField");

        actions.waitForElementClickable("jira.create.priorityMedium");
        actions.clickElement("jira.create.priorityMedium");
        actions.waitForElementClickable("jira.create.priorityHighest");
        actions.clickElement("jira.create.priorityHighest");

        actions.waitForElementClickable("jira.create.buttonCreateIssue");
        actions.clickElement("jira.create.buttonCreateIssue");

        actions.waitForElementVisible("//div[@data-testid='create-button-wrapper']");
        actions.assertElementPresent("//div[@data-testid='create-button-wrapper']");


    }
}
