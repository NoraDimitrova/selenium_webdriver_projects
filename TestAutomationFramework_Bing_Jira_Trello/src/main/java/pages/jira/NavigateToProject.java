package pages.jira;

import org.openqa.selenium.WebDriver;

public class NavigateToProject extends BaseJiraPage{
    public NavigateToProject(WebDriver driver) {
        super(driver, "jira.navigateToProjectUrl");
    }

    public void navigate(){
        actions.waitForElementVisible("jira.loginJiraPage.selectProject");
        actions.waitForElementClickable("jira.loginJiraPage.selectProject");
        actions.clickElement("jira.loginJiraPage.selectProject");

        actions.waitForElementVisible("jira.loginJiraPage.buttonProject");
        actions.clickElement("jira.loginJiraPage.buttonProject");

        actions.waitForElementVisible("//span[@style='background-image: url(\"https://noradimitrova.atlassian." +
                "net/rest/api/2/universal_avatar/view/type/project/avatar/10419?size=medium\"); border-radius: 2px;']");
        actions.clickElement("//span[@style='background-image: url(\"https://noradimitrova.atlassian.net/rest/api/2" +
                "/universal_avatar/view/type/project/avatar/10419?size=medium\"); border-radius: 2px;']");

        actions.waitForElementVisible("jira.loginJiraPage.keyBoard");
        actions.assertElementPresent("jira.loginJiraPage.keyBoard");

    }
}
