package pages.jira;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LinkBugToStory extends BaseJiraPage {
    public LinkBugToStory(WebDriver driver) {
        super(driver, "jira.navigateToStory");
    }

    public void linkBugToStory(String user) {
        String bugKey = Utils.getConfigPropertyByKey("jira.users." + user + ".bugKey");

        navigateToPage();

        actions.waitForElementClickable("jira.linkBugToStory.linkIssueButton");
        actions.clickElement("jira.linkBugToStory.linkIssueButton");
        actions.waitForElementPresent("jira.linkBugToStory.searchForIssueField");
        actions.typeValueInField(bugKey, "jira.linkBugToStory.searchForIssueField");
        actions.pressKey(Keys.ENTER);

        actions.waitForElementClickable("jira.linkBugToStory.buttonFinalLink");
        actions.waitForElementPresent("jira.linkBugToStory.buttonFinalLink");
        actions.clickElement("jira.linkBugToStory.buttonFinalLink");

    }
}

