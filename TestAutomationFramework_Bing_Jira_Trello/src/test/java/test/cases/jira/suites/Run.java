package test.cases.jira.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.cases.jira.CreateBugTest;
import test.cases.jira.CreateStoryTest;
import test.cases.jira.JiraTest;
import test.cases.jira.LinkBugToStoryTest;


@Suite.SuiteClasses({ CreateStoryTest.class, CreateBugTest.class, LinkBugToStoryTest.class})
@RunWith(Suite.class)

public class Run {
}
