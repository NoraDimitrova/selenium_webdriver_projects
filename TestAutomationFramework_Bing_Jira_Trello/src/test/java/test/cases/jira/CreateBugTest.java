package test.cases.jira;

import org.junit.Assert;
import org.junit.Test;
import pages.jira.CreateBug;
import pages.jira.LoginJiraPage;
import pages.jira.NavigateToProject;

public class CreateBugTest extends BaseJira {

    @Test
    public void testCreateBug() {
        LoginJiraPage loginJiraPage = new LoginJiraPage(actions.getDriver());
        loginJiraPage.loginJira("user");

        NavigateToProject navigateToProject=new NavigateToProject(actions.getDriver());
        navigateToProject.navigate();

        CreateBug createBug = new CreateBug(actions.getDriver());
        createBug.createBug("user");
        //assert
        Assert.assertEquals("Bug is not created", "//div[@data-testid='create-button-wrapper']", "//div[@data-testid='create-button-wrapper']");

    }
}
