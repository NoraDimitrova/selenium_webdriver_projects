package test.cases.jira;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;

import org.junit.BeforeClass;


public class BaseJira {
    UserActions actions = new UserActions();


    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("jira.url");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }
}
