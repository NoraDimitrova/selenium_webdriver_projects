package test.cases.jira;

import org.junit.Assert;
import org.junit.Test;
import pages.jira.CreateStory;
import pages.jira.LoginJiraPage;
import pages.jira.NavigateToProject;

public class CreateStoryTest extends BaseJira{

    @Test
    public void testCreateStory() {
        LoginJiraPage loginJiraPage = new LoginJiraPage(actions.getDriver());
        loginJiraPage.loginJira("user");

        NavigateToProject navigateToProject=new NavigateToProject(actions.getDriver());
        navigateToProject.navigate();

        CreateStory createStory = new CreateStory(actions.getDriver());
        createStory.createStory("user");
        //assert
        Assert.assertEquals("Story is not created", "//div[@data-testid='create-button-wrapper']", "//div[@data-testid='create-button-wrapper']");
    }
}
