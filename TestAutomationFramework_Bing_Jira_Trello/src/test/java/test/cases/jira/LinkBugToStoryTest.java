package test.cases.jira;

import org.junit.Test;
import pages.jira.LinkBugToStory;
import pages.jira.LoginJiraPage;
import pages.jira.NavigateToProject;

public class LinkBugToStoryTest extends BaseJira {

    @Test
    public void testLinBugToStory() {

        LoginJiraPage loginJiraPage = new LoginJiraPage(actions.getDriver());
        loginJiraPage.loginJira("user");

        NavigateToProject navigateToProject=new NavigateToProject(actions.getDriver());
        navigateToProject.navigate();

        LinkBugToStory linkBugToStory = new LinkBugToStory(actions.getDriver());
        linkBugToStory.linkBugToStory("user");

    }
}
