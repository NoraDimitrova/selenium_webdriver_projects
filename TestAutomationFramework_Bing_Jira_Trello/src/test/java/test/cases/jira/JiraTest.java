package test.cases.jira;

import org.junit.Assert;
import org.junit.Test;
import pages.jira.*;

public class JiraTest extends BaseJira {

    @Test
    public void testLoginJiraSuccessfully() {
        LoginJiraPage loginJiraPage = new LoginJiraPage(actions.getDriver());
        loginJiraPage.loginJira("user");

        NavigateToProject navigateToProject=new NavigateToProject(actions.getDriver());
        navigateToProject.navigate();

        //assert
        Assert.assertEquals("Failed to navigate to board", "//h1[@class='pazoq0-7 jgsowA']",
                "//h1[@class='pazoq0-7 jgsowA']");

    }


    @Test
    public void testCreateStory() {

        CreateStory createStory = new CreateStory(actions.getDriver());
        createStory.createStory("user");

        //assert
        Assert.assertEquals("Story is not created", "//div[@data-testid='create-button-wrapper']", "//div[@data-testid='create-button-wrapper']");
    }

    @Test
    public void testCreateBug() {

        CreateBug createBug = new CreateBug(actions.getDriver());
        createBug.createBug("user");

        //assert
        Assert.assertEquals("Bug is not created", "//div[@data-testid='create-button-wrapper']", "//div[@data-testid='create-button-wrapper']");

    }

}
