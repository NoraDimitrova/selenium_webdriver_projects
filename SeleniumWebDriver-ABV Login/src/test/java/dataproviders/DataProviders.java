package dataproviders;

import org.testng.annotations.DataProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DataProviders {

    @DataProvider
    public static Object[][] mostCommonPass(){
        Object [][] dataset = new Object[100][1];
        BufferedReader reader;
        try {
            //TODO
            reader = new BufferedReader(new FileReader
                    ("C:\\Users\\Nora Dimitrova\\Desktop\\SELENIUM\\SeleniumWebDriver-ABV Login\\src\\test\\resources\\100pass.txt"));
            int i=0;
            String line = reader.readLine();
            while (line != null){
                dataset[i][0] = line;
                i++;
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e){
            e.printStackTrace();
        }
        return dataset;
    }
}
