package test.cases;

import dataproviders.DataProviders;
import org.junit.Assert;
import org.testng.annotations.Test;
import pages.LoginABV;

public class LoginABVTest extends BaseTestSetup {


    @Test(priority = 1)
    public void acceptCookies() {
        LoginABV login = new LoginABV(actions.getDriver());
        login.cookies();
    }

    @Test(priority = 2, dataProvider = "mostCommonPass", dataProviderClass = DataProviders.class)
    public void CheckPassword(String password) {
        LoginABV login = new LoginABV(actions.getDriver());
        login.checkPass(password);
        login.validate();
        Assert.assertEquals("Message", "//div[@class='abv-button']", "//div[@class='abv-button']");

    }
}
