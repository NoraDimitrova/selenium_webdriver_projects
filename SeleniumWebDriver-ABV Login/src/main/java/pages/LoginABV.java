package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class LoginABV extends BasePageABV {

    public LoginABV(WebDriver driver) {
        super(driver, "home.page");
    }

    public void cookies() {

        navigateToPage();

        actions.waitFor(15000);
        actions.waitForElementVisible("abv.login.logo");
        actions.getDriver().switchTo().frame("abv-GDPR-frame");
        actions.waitFor(15000);
        actions.getDriver().switchTo().frame("gdpr-consent-notice");
        actions.clickElement("abv.login.acceptCookies");

    }

    public void checkPass(String password) {

        String email = Utils.getConfigPropertyByKey("email");

        navigateToPage();

        actions.typeValueInField(email, "abv.login.email");
        actions.typeValueInField(password, "abv.login.password");
        actions.clickElement("abv.login.loginButton");
        actions.waitFor(20000);
        actions.waitForElementVisible("//td[contains(text(),'\" + email + \"')]");
        actions.assertElementPresent("//td[contains(text(),'" + email + "')]");
    }

    public void validate() {

        actions.waitForElementVisible("//div[@class='abv-button']");
        actions.assertElementPresent("//div[@class='abv-button']");
        actions.assertElementPresent("//td[@class='foldersCell foldersSelectedCell foldersHoveredCell']");
    }

}
