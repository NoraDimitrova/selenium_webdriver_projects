package pages;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public abstract class BasePageABV extends BasePage {
    public BasePageABV(WebDriver driver, String urlKey) {
        super(driver, urlKey);
    }
}
