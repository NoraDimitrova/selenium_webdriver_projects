**Instructions**

1. Clone repository
2. Open `Telerik Academy Staging Forum - exam` as a IntelliJ IDEA Project
3. Build
4. Put your email and password for staging forum in resources/config.properties file
5. Run tests in logical order - Run 'All Tests' or by test class
6. To run unlikeTopicWhenClickHeartIconTest() separately invoke login()
7. Application under Test - [Telerik Academy Staging Forum](https://stage-forum.telerikacademy.com/)