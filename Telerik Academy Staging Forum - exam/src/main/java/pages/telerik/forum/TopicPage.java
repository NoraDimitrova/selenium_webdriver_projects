package pages.telerik.forum;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class TopicPage extends BaseForumPage {

    public TopicPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void commentTopic(String topicName, String comment) {

        navigateToPage();

        actions.clickElement("home.page.search.button");
        actions.clickElement("home.page.search.input");
        actions.typeValueInField(topicName, "home.page.search.input");
        actions.pressKey(Keys.ENTER);
        actions.waitForElementClickable("home.page.search.result");
        actions.clickElement("home.page.search.result");

        if (actions.isElementVisible("comment.cancel.button")) {
            actions.clickElement("comment.cancel.button");
            actions.pressKey(Keys.ENTER);
        }
        actions.waitForElementClickable("topic.page.reply");
        actions.clickElement("topic.page.reply");
        actions.clickElement("comment.description");
        actions.typeValueInField(comment, "comment.description");
        actions.pressKeyCopyAll();
        actions.clickElement("comment.block.quote.button");
        actions.clickElement("create.item.button");

        actions.waitForElementVisible("comment.time.created");
        actions.assertElementAttribute("comment.time.created", "innerText", "1m");
        actions.assertElementAttribute("comment.content", "innerText", "Hi, I am Didi, and I did it");
    }

    public void likeTopic(String topicName) {

        navigateToPage();

        actions.clickElement("home.page.search.button");
        actions.clickElement("home.page.search.input");
        actions.typeValueInField(topicName, "home.page.search.input");
        actions.pressKey(Keys.ENTER);

        actions.waitForElementClickable("home.page.search.result");
        actions.clickElement("home.page.search.result");

        if (actions.isElementVisible("topic.page.like.post.button")) {
            actions.clickElement("topic.page.like.post.button");
        } else {
            actions.clickElement("topic.page.dislike.post.button");
            actions.clickElement("topic.page.like.post.button");
        }

        actions.waitForElementVisible("topic.page.dislike.post.button");
        actions.assertElementPresent("topic.page.dislike.post.button");
    }

    public void unlikeTopic(String topicName) {

        navigateToPage();

        actions.clickElement("home.page.search.button");
        actions.clickElement("home.page.search.input");
        actions.typeValueInField(topicName, "home.page.search.input");
        actions.pressKey(Keys.ENTER);

        actions.waitForElementClickable("home.page.search.result");
        actions.clickElement("home.page.search.result");

        if (actions.isElementVisible("topic.page.like.post.button")) {
            actions.clickElement("topic.page.like.post.button");
            actions.clickElement("topic.page.dislike.post.button");
        } else {
            actions.clickElement("topic.page.dislike.post.button");
        }

        actions.waitForElementVisible("topic.page.like.post.button");
        actions.assertElementPresent("topic.page.like.post.button");
    }
}
