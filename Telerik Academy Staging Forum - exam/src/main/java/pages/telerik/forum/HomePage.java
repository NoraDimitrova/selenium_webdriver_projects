package pages.telerik.forum;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import java.util.Random;

import static java.lang.String.format;

public class HomePage extends BaseForumPage {

    private String topicTitle;
    private String topicDescription = "";
    Random random = new Random();
    int randomCount;

    public HomePage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void createTopic() {

        navigateToPage();

        generateTitle();
        generateDescription();

        actions.clickElement("new.topic.button");
        actions.waitForElementClickable("topic.title");
        actions.typeValueInField(topicTitle, "topic.title");
        actions.typeValueInField(topicDescription, "topic.description");
        actions.clickElement("create.item.button");

        actions.waitForElementClickable("topic.page.title");

        actions.assertElementAttribute("topic.page.title", "innerText", topicTitle);

        System.out.printf("Topic with title %s is created", topicTitle);

        deleteTopic();
    }

    private void generateTitle() {

        randomCount = random.nextInt(16);
        String topicNumber = "";

        while (randomCount >= 0) {
            String randomNumber = RandomStringUtils.randomNumeric(14);
            topicNumber = topicNumber.concat(randomNumber).concat(" ");
            randomCount--;
        }
        topicTitle = format("Topic %s", topicNumber.trim());
    }

    private void generateDescription() {

        randomCount = random.nextInt(20);
        while (randomCount >= 0) {
            String randomStr = RandomStringUtils.randomAlphabetic(33);
            String randomNumber = RandomStringUtils.randomNumeric(33);
            String randomAlphanumeric = RandomStringUtils.randomAlphanumeric(33);
            topicDescription = topicDescription.concat(randomStr).concat(" ").concat(randomNumber).concat(" ")
                .concat(randomAlphanumeric).concat("\n");
            randomCount--;
        }
    }

    private void deleteTopic() {

        actions.clickElement("topic.page.edit.topic");
        actions.clickElement("topic.page.edit.title");
        actions.typeValueInField("Test Topic", "topic.page.edit.title");
        actions.clickElement("topic.page.save.edit");
        actions.clickElement("topic.page.button.show.more");
        actions.clickElement("topic.page.button.delete.topic");

        actions.waitForElementVisible("new.topic.button");
        actions.assertElementPresent("new.topic.button");
    }
}
