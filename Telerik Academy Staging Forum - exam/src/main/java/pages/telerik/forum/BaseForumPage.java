
package pages.telerik.forum;

import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public abstract class BaseForumPage extends BasePage {

    public BaseForumPage(WebDriver driver, String urlKey) {
        super(driver, urlKey);
    }
}
