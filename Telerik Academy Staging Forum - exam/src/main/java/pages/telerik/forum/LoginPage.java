package pages.telerik.forum;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseForumPage{

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void loginUser(){

        String email = getConfigPropertyByKey("email");
        String password = getConfigPropertyByKey("password");

        navigateToPage();

        actions.clickElement("login.button");
        actions.waitForElementClickable("login.button.signIn");

        actions.typeValueInField(email, "login.email");
        actions.typeValueInField(password, "login.password");
        actions.clickElement("login.button.signIn");

        actions.waitForElementClickable("home.page.user.info");
        actions.assertElementPresent("home.page.user.info");
    }
}
