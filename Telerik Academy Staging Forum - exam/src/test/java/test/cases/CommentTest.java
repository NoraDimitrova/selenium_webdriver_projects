package test.cases;

import pages.telerik.forum.TopicPage;

import org.junit.Test;

public class CommentTest extends BaseTestSetup {

    @Test
    public void commentWhenClickReplyTest() {

        login();

        TopicPage topicPage = new TopicPage(actions.getDriver());
        topicPage.commentTopic(topicName, comment);
    }
}
