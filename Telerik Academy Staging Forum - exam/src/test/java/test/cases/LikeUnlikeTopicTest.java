package test.cases;

import pages.telerik.forum.TopicPage;

import org.junit.Test;

public class LikeUnlikeTopicTest extends BaseTestSetup {

    TopicPage topicPage = new TopicPage(actions.getDriver());

    @Test
    public void likeTopicWhenClickHeartIconTest() {

        login();
        topicPage.likeTopic(topicName);
    }

    @Test
    public void unlikeTopicWhenClickHeartIconTest() {

        //login();
        topicPage.unlikeTopic(topicName);
    }
}
