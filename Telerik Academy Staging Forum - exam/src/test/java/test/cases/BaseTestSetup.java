package test.cases;

import pages.telerik.forum.LoginPage;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTestSetup {

    UserActions actions = new UserActions();
    String topicName = "New Test Demo Topic";
    String comment = "Hi, I am Didi, and I did it :smile: :partying_face: :beers: :see_no_evil: :hear_no_evil: :speak_no_evil: :heart:";


    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("home.page");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }

    public void login() {

        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser();
    }
}
